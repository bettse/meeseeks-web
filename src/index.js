import React from 'react';
import ReactDOM from 'react-dom';
import { Connector } from 'mqtt-react';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Connector mqttProps='wss://webapp:QkvUNdZD@m14.cloudmqtt.com:37054'>
    <App />
  </Connector>
, document.getElementById('root'));
registerServiceWorker();
