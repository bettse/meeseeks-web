import React, { Component } from 'react';
import { subscribe } from 'mqtt-react';
import box from './box.png';
import './App.css';
import soundFile from './click.wav';

class App extends Component {
  constructor(props){
    super(props);
    this.sound = new Audio(soundFile);
  }

  sendMessage(e) {
      e.preventDefault();
      const { mqtt } = this.props;
      this.sound.play();
      mqtt.publish('meeseeks', JSON.stringify({version: 1, action: 'random'}));
  }

  render() {
    const { mqtt, data } = this.props
    const { connected } = mqtt
    const lastState = data.filter(({state}) => state)[0]
    const className = (lastState && lastState.state) || "idle"
    return (
      <div className="App">
        <header className="App-header">
          { connected && <img src={box} className={className} alt="logo" onClick={this.sendMessage.bind(this)} /> }
          { !connected &&
            <div>
              <h1 className="App-title">Connecting to MQTT ...</h1>
              <p className="App-intro"> Can take up to 10 seconds</p>
            </div>
          }
        </header>
      </div>
    );
  }
}

export default subscribe({topic: 'meeseeks'})(App);
